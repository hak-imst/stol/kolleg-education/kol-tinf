#!/bin/bash

if [ -n $1 ]; then
    echo "Es wurde folgendes Argument übergeben: $1"
fi

nc -w 1 -z www.google.de 80 >/dev/null 2>&1
success_80=$?

if [ $success_80 -eq 0 ]; then
    echo "Host has Internet Connection"
    ping -w 1 -c 1 www.google.de >/dev/null 2>&1
    success_ping=$?
    if [ $success_ping -gt 0 ]; then
        echo "No ICMP allowed"
    else 
        echo "ICMP allowed"
    fi
else
    echo "Host has no Internet Connection"
fi
