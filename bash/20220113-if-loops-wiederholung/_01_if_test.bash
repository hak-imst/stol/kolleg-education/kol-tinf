#!/bin/bash

ls /home > /dev/null

echo $? 

ls /eikaufen > /dev/null

echo $? 

echo "Versuch 1"
ls /eikaufen 2> /dev/null

# Folgendes ergibt "command not found", weil "2" kein gültiger Befehl ist
if $? ; then

    echo "Befehl erfolgreich"

else 

    echo "Fehler!"

fi

echo "Versuch 2"
ls /eikaufen 2> /dev/null

# Folgendes ergibt "command not found", weil "2" kein gültiger Befehl ist
if echo $? ; then

    echo "Befehl erfolgreich"

else 

    echo "Fehler!"

fi

# Achtung, hier natürlich keine [], weil hier nicht test gebraucht wird
if ls /einkaufen 2> /dev/null ; then

    echo "Befehl erfolgreich"

else 

    echo "Fehler!"

fi

echo "Zuerst Befehl ausführen und dann prüfen, ob erfolgreich"

ls /seppi > /dev/null
returnCode=$?

# Alternativ: if test $? -eq 0 ; then
if [ $returnCode -eq 0 ] ; then

    echo "Befehl erfolgreich"

else 

    echo "Fehler!"

fi