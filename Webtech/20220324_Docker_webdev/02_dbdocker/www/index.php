<!-- source: https://www.sitepoint.com/a-minimal-html-document-html5-edition/ -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mein Titel</title>
  </head>
  <body>
    Echo Hello World!!

    <h1>PHP</h1>

    <?php
      // Das Kommentar
      for($i=0;$i<3;$i++){
        echo "<p>Hier steht der mit <b>PHP</b> geprintete Absatz</p>";
      }
      
    ?>
  </body>
</html>
