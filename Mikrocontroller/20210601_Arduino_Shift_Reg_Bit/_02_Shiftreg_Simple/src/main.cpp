#include <Arduino.h>

int ds = 5;
int sh_cp = 6; // Pin, der gelesen werden soll: Pin A3
int st_cp = 7; // Variable, die den gelesenen Wert speichert

void setup() {
  pinMode(ds,OUTPUT);
  pinMode(sh_cp,OUTPUT);
  pinMode(st_cp,OUTPUT);
  digitalWrite(ds,LOW);
  digitalWrite(sh_cp,LOW);
  digitalWrite(st_cp,LOW);
}

void loop() {

  digitalWrite(ds,HIGH);
  digitalWrite(sh_cp,HIGH);
  digitalWrite(sh_cp,LOW);  
  
  digitalWrite(ds,LOW);
  digitalWrite(sh_cp,HIGH);
  digitalWrite(sh_cp,LOW);  
  
  digitalWrite(ds,LOW);
  digitalWrite(sh_cp,HIGH);
  digitalWrite(sh_cp,LOW);

  digitalWrite(st_cp,HIGH);
  digitalWrite(st_cp,LOW);

  delay(1000);
  
}