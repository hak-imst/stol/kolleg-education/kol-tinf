# Schieberegister

## Flipflop

Ein [Schieberegister](https://www.elektronik-kompendium.de/sites/dig/0210211.htm) ist auf [Flipflops](https://de.wikipedia.org/wiki/Flipflop) aufgebaut. Dabei handelt es sich um eine elektronische Schaltung, die zwei stabile Zustände als Ausgangssignal besitzt (ein Bit). Dadurch können Daten auf unbegrenzte Zeit gespeichert werden. Der Speicher ist jedoch flüchtig, also muss die Spannung aufrecht bleiben.

Fliplops werden zum Beispiel in Mikroprozessor, statischem RAM und auch im Schieberegister eingesetzt. 

| Name | Schaltzeichen | Signal-Zeit-Diagramm | Funktionstabelle  |
|---|---|---|---|
| RS-Flipflop mit Taktpegel­steuerung | ![SER Flipflop](images/Gated_SR_flip-flop_Symbol.svg.png) | ![SZ Diagramm](images/SR_latch_impulse_diagram.png) |  ![Schieberegister Logik](images/shiftreg_logic.png) |


|   |
|---|
|  ![Schieberegister Schema](images/shiftreg_schema.png) |
| Schieberegister mit taktpegelgesteuertem RS-Flipflop aufgebaut  |






 

Die Schaltung für ein 74HC595 ist in der [Arduino Homepage](https://www.arduino.cc/en/tutorial/ShiftOut) nachzulesen. 

 


 

|   |
|---|
|  ![74HC595](images/74HC595.png)) |
| 74HC595 Mapping   |