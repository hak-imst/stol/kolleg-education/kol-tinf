## Unterlagen 

 

Die meisten Sensoren ändern in Abhängigkeit eines Umweltparameters den Widerstand. Beispiel LDR (Light Density Resistor), der den Widerstand verringert, wenn Licht auf ihn fällt. 

 

Die Spannungsteilerschaltung wird benötigt, weil der Arduino nur eine Spannungsänderung messen kann. Durch einen Widerstand ändert sich die Spannung eines Gesamtsystems nicht. Die Spannung teilt sich aber gleichmäßig zwischen mehreren in Serie geschalteten Widerständen auf. 

 

[Parallelschaltung von Widerständen](https://www.elektronik-kompendium.de/sites/slt/0110192.htm) 

[Reihenschaltung von Widerständen](https://www.elektronik-kompendium.de/sites/slt/0110191.htm) 

[Spannungsteiler Berechnung](https://www.peacesoftware.de/einigewerte/spannungsteiler.html) 

[analogRead() Funktion Arduino](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) 

 

## Schaltungen 

 

Als Widerstand empfiehlt sich 10K Ohm 

![Spannungsteilerschaltung](images/spannungsteiler.png)