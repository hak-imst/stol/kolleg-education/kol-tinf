[Quelle für folgende Theorie]([http://einstein.informatik.uni-oldenburg.de/rechnernetze/digitali.htm) 

![Diskretisierung](images/Diskretisierung.png) 

## Digitalisierung 

Die moderne Informationstechnik unterscheidet sich von der früheren, etwa dem klassischen Telephon, durch die Digitalisierung der Information. Der Unterschied zwischen "**analog**" und "**digital**" liegt darin, dass analoge Größen jeden Wert auf einer bestimmten Skala annehmen können, während digitale Größen nur endlich viele Werte annehmen können, in der Regel 2 (digital) oder eine andere endlich Zahl, was wir im folgenden allgemein diskret (discrete) nennen. Daher ist eine diskrete Größe im Prinzip ungenauer. Dennoch hat sich dieses technologische Prinzip heute mit großem Erfolg durchgesetzt. 

Der Grund liegt im wesentlich darin, dass ein analoger Wert nur im Prinzip beliebig genau sein kann. Schon die Speicherung, aber noch mehr die Übertragung der Information führt stets zu einer Verfälschung der Information, mit dem Ergebnis, dass Fehler auftreten. Da Information durch Energie (elektrische Ladung, Licht) dargestellt und daher ständig verändert wird, muss die Information ständig regeneriert werden, in der Regel durch Verstärkung. Jede Verstärkung der gewünschten Information führt aber auch zu einer Verstärkung der Verfälschung dieser Information. Deshalb waren früher "Ferngespräche", die über viele Vermittlungsstellen geführt werden mussten, nur schwer verständlich, weil der Empfänger vielfach verstärkte Verfälschungen erhielt. 

### Beispiel 

Ein Signalwert betrage 10 V. Auf jeder Leitung werde der Signalwert um 50 % gedämpft und um 5 % verfälscht. Vor dem ersten Regenerator beträgt der Signalwert somit 5,5 V, nach Verstärkung 11 V. Nach der zweiten Verstärkung erhalten wir (11 V/2 + 0,55 V)*2 = 12,1 V. Man erhält die folgende Tabelle: 

|  0 | 1  | 2  | 3  |  4 |  5 | 6  | 7  |
|---|---|---|---|---|---|---|---|
| 10   | 11  |  12,1 | 13,31  |  14,64 |  16,11 |  17,72 | 19,49  |


Man sieht, dass nach der achten Verstärkung bereits der Fehler einer 5 prozentigen Verfälschungen das Signal zu 100 % verfälscht hat. 

## Diskretisierung

Im Gegensatz hierzu geht man bei der Diskretisierung davon aus, dass ein Signal nur wenige Werte besitzt. Ein Fehler bei einer Übertragung, soweit er nicht allzu gravierend ist, kann vom Empfänger leicht erkannt und entsprechend korrigiert werden. 

### Beispiel 

Digitale Signale besitzen nur zwei Werte, die meistens mit 0 und 1 bezeichnet werden. Beispielsweise könnte der 0-Wert mit einer Spannung im Bereich von 0 V bis 1 V dargestellt werden, der 1-Wert mit einer Spannung von 2 V oder größer, wobei der übliche Wert 5 V betrage. Jeder Wert zwischen 1 und 2 V ist undefiniert. Wird jetzt ein Signal von 5 V um 50 % gedämpft, so erhalten wir 2,5 V; wird es zusätzlich um 5 % verfälscht, so liegt es noch immer im Bereich von 2,25 V bis 2,75 V. Nach einer Verstärkung sollte es aber wieder einen Wert von ca. 5 V erhalten, so dass die Information, nämlich 0-Wert oder 1-Wert, unverfälscht zum Empfänger übertragen wird. Der Empfänger kann das regenerierte Signal weitersenden, wo es wieder regeneriert wird usw., ohne dass überhaupt eine Verfälschung der Information auftritt. 

Die meiste Information, z.B. Sprachinformation, besteht aus sehr vielen verschiedenen Werten. Um diese zu diskretisieren, wird zunächst ein gewisses Raster vorgegeben, auf welches die analogen Signale abgebildet werden. Z.B. ließen sich die Werte eines Sprachsignals von –10 V bis +10 V auf 201 diskrete Werte (-10 V, -9,9 V, -9,8 V, ... –0,1 V, 0.0 V, 0,1 V, ... 9,9 V, 10,0 V) abbilden. Die Rundung werde beispielsweise durch den "nahesten diskreten Wert" festgelegt. Diese Werte könnten jetzt durch digitale Werte dargestellt werden. Mit acht 1-Bit Werten lassen sich 28=256 verschiedene Werte unterscheiden. Daher lässt sich jeder analoge Wert aus diesem Beispiel auf ein Byte abbilden. Diese Bytes können jetzt bitweise digital übertragen werden, und der Empfänger kann jedes einzelne Bit stets wieder genau restaurieren, also einen völlig unverfälschten Wert erhalten. Dieses kann auch über mehrere Zwischenverstärker geschehen, so dass insgesamt die digitale Übertragung analoger Signale eine relevante Verbesserung der Qualität gegenüber der analogen Übertragung bedeutet. 

Dieses Prinzip wird, mit einigen technischen Verbesserungen, im ISDN verwendet, welches heute allgemein sowohl zur Sprach- als auch zur Datenübertragung zur Verfügung steht. 

Auch die Speicherung analoger Information wird durch eine Digitalisierung erst wirtschaftlich möglich. Wurden früher spezielle Geräte verwendet, um Sprachsignale zu speichern, beispielsweise Tonbandgeräte, so wird etwa in der Zeitansage heute ausschließlich digital gespeicherte Sprache verwendet. Selbst Anrufbeantworter speichern heute die Ansagen und die Nachrichten wirtschaftlicher digital als analog. Mittlerweile kann durch die Entwicklung der Speichertechnik und Verbesserung von Kodierungsverfahren auch Video, etwas normales Fernsehen, digital gespeichert werden.  