## Getimte Ausführung

Es wird anstatt von Kontextwechseln getimte Ausführung durchgeführt.   

[Library für getimte Methoden](https://playground.arduino.cc/Code/TimedAction/) 

## Protothreading

Protothreading ist nicht preemtive und kommt ohne Kontextwechsel durch das Betriebssystem aus. Es werden C Macros anstatt von Funktionen verwendet, um es zu ermöglichen die flow control zu verändern.

[Adam Dunkels protothreading C Library](http://dunkels.com/adam/pt/expansion.html)